@extends('layouts.front')

@section('title')
    Marazzo
@endsection

@section('body')
    <div class="col-xs-12 col-sm-12 col-md-4 sidebar">
        {{-- categories --}}
        <div class="side-menu animate-dropdown outer-bottom-xs">
            <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
            @foreach ($categories as $category)
                <nav class="yamm megamenu-horizontal">
                    <ul class="nav">
                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon fa fa-shopping-bag"
                                    aria-hidden="true"></i>{{ $category->name }}</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-12">
                                            <ul class="links list-unstyled">
                                                @foreach ($subcategories as $subcategory)
                                                    @if ($subcategory->category_id == $category->id)
                                                        <li>
                                                            <a href="/front">{{ $subcategory->name }}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            @endforeach

        </div>
        {{-- End categories --}}


    </div>
    <!-- /.sidemenu-holder -->
    <!-- ============================================== SIDEBAR : END ============================================== -->

    <!-- ============================================== CONTENT ============================================== -->
    <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
        <!-- ========================================== SECTION – HERO ========================================= -->
        <div id="hero">
            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                <div class="item"
                    style="background-image: url({{ asset('front-assets/images/sliders/01.jpg') }});">
                    <div class="container-fluid">
                        <div class="caption bg-color vertical-center text-left">
                            <div class="slider-header fadeInDown-1">Top Brands</div>
                            <div class="big-text fadeInDown-1"> New Collections </div>
                            <div class="excerpt fadeInDown-2 hidden-xs"> <span>Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit.</span> </div>
                            <div class="button-holder fadeInDown-3"> <a href="index6c11.html?page=single-product"
                                    class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div>
                        </div>
                        <!-- /.caption -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->

                <div class="item"
                    style="background-image: url({{ asset('front-assets/images/sliders/02.jpg') }});">
                    <div class="container-fluid">
                        <div class="caption bg-color vertical-center text-left">
                            <div class="slider-header fadeInDown-1">Spring 2018</div>
                            <div class="big-text fadeInDown-1"> Women Fashion </div>
                            <div class="excerpt fadeInDown-2 hidden-xs"> <span>Nemo enim ipsam voluptatem quia voluptas sit
                                    aspernatur aut odit aut fugit</span> </div>
                            <div class="button-holder fadeInDown-3"> <a href="index6c11.html?page=single-product"
                                    class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div>
                        </div>
                        <!-- /.caption -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.item -->
            </div>
            <!-- /.owl-carousel -->
        </div>

        <!-- ========================================= SECTION – HERO : END ========================================= -->


        <!-- ============================================== SCROLL TABS ============================================== -->
        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs">
            <div class="more-info-tab clearfix ">
            <h3 class="new-product-title pull-left">New Products</h3>
            <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                <li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">All</a></li>
                <li><a data-transition-type="backSlide" href="#smartphone" data-toggle="tab">Clothing</a></li>
                <li><a data-transition-type="backSlide" href="#laptop" data-toggle="tab">Electronics</a></li>
                <li><a data-transition-type="backSlide" href="#apple" data-toggle="tab">Shoes</a></li>
            </ul>
            <!-- /.nav-tabs -->
            </div>
            <div class="tab-content outer-top-xs">
                <div class="tab-pane in active" id="all">
                        <div class="product-slider">
                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                                @foreach ($products as $product)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="/detail/{{ $product->slug }}">
                                                            <img src="{{ $product->product_photo }}" alt="">
                                                            <img src="{{ $product->product_photo }}" alt=""
                                                                class="hover-image">
                                                        </a>
                                                    </div>
                                                    <!-- /.image -->
                                                </div>
                                                <!-- /.product-image -->
                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="detail.html">{{ $product->name }}</a>
                                                    </h3>
                                                    <div class="description"></div>
                                                    <div class="product-price"> <span class="price">
                                                            Rs.{{ $product->price }}</span>
                                                        <span class="price-before-discount">$ 800</span>
                                                    </div>
                                                    <!-- /.product-price -->
                                                </div>
                                                <!-- /.cart -->
                                            </div>
                                            <!-- /.product -->
                                        </div>
                                        <!-- /.products -->
                                    </div>
                                @endforeach
                                <!-- /.item -->
                            </div>
                            <!-- /.home-owl-carousel -->
                        </div>
                        <!-- /.product-slider -->
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="men">
                    <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                            @foreach ($products as $product)
                                @if ($product->category_id == 1)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="/detail">
                                                            <img src="{{ $product->product_photo }}" alt="">
                                                            <img src="{{ $product->product_photo }}" alt=""
                                                                class="hover-image">
                                                        </a>
                                                    </div>
                                                    <!-- /.image -->
                                                </div>
                                                <!-- /.product-image -->
                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="detail.html">{{ $product->name }}</a></h3>
                                                    <div class="description"></div>
                                                    <div class="product-price">
                                                        <span class="price">Rs.{{ $product->price }}</span>
                                                        <span class="price-before-discount">$ 800</span>
                                                    </div>
                                                    <!-- /.product-price -->
                                                </div>

                                            </div>
                                            <!-- /.product -->
                                        </div>
                                        <!-- /.products -->
                                    </div>
                                @endif
                            @endforeach
                            <!-- /.item -->
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="women">
                    <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                            @foreach ($products as $product)
                                @if ($product->category_id == 2)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="/detail">
                                                            <img src="{{ $product->product_photo }}" alt="">
                                                            <img src="{{ $product->product_photo }}" alt="" class="hover-image">
                                                        </a>
                                                    </div>
                                                    <!-- /.image -->
                                                </div>
                                                <!-- /.product-image -->
                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="v">{{ $product->name }}</a></h3>
                                                    <div class="description"></div>
                                                    <div class="product-price">
                                                        <span class="price">${{ $product->price }}</span>
                                                        <span class="price-before-discount">$ 800</span>
                                                    </div>
                                                    <!-- /.product-price -->
                                                </div>
                                                <!-- /.cart -->
                                            </div>
                                            <!-- /.product -->
                                        </div>
                                        <!-- /.products -->
                                    </div>
                                @endif
                            @endforeach

                        </div>
                        <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="electronics">
                    <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                            @foreach ($products as $product)
                                @if ($product->category_id == 3)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="/detail">
                                                            <img src="{{ $product->product_photo }}"
                                                                alt="">
                                                            <img src="{{ $product->product_photo }}"
                                                                alt="" class="hover-image">
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- /.product-image -->
                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="/detail">{{ $product->name }}</a></h3>
                                                    <div class="description"></div>
                                                    <div class="product-price">
                                                        <span class="price"> $450.99 </span>
                                                        <span class="price-before-discount">$800</span>
                                                    </div>
                                                    <!-- /.product-price -->
                                                </div>
                                                <!-- /.product-info -->
                                            </div>
                                            <!-- /.product -->
                                        </div>
                                        <!-- /.products -->
                                    </div>
                                @endif
                            @endforeach
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="games">
                    <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                            @foreach ($products as $product)
                                @if ($product->category_id == 4)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="/detail                                                        <img src="{{ $product->product_photo }}" alt="">
                                                            <img src="{{ $product->product_photo }}" alt=""class="hover-image">
                                                        </a>
                                                    </div>
                                                    <!-- /.image -->
                                                </div>
                                                <!-- /.product-image -->
                                                <div class="product-info text-left">
                                                    <h3 class="name">
                                                        <a href="/detail">{{ $product->name }}</a>
                                                    </h3>
                                                    <div class="description"></div>
                                                    <div class="product-price"> <span class="price">$ {{ $product->price }}</span>
                                                        <span class="price-before-discount">$ 800</span>
                                                    </div>
                                                    <!-- /.product-price -->
                                                </div>
                                                <!-- /.cart -->
                                            </div>
                                            <!-- /.product -->
                                        </div>
                                        <!-- /.products -->
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.scroll-tabs -->
        <!-- ============================================== SCROLL TABS : END ============================================== -->
        <!-- ============================================== WIDE PRODUCTS ============================================== -->
        <div class="wide-banners outer-bottom-xs">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="wide-banner cnt-strip">
                        <div class="image"> <img class="img-responsive"
                                src="{{ asset('front-assets/images/banners/home-banner1.jpg') }}" alt=""> </div>
                    </div>
                    <!-- /.wide-banner -->
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="wide-banner cnt-strip">
                        <div class="image"> <img class="img-responsive"
                                src="{{ asset('front-assets/images/banners/home-banner3.jpg') }}" alt=""> </div>
                    </div>
                    <!-- /.wide-banner -->
                </div>

                <!-- /.col -->
                <div class="col-md-4 col-sm-4">
                    <div class="wide-banner cnt-strip">
                        <div class="image"> <img class="img-responsive"
                                src="{{ asset('front-assets/images/banners/home-banner2.jpg') }}" alt=""> </div>
                    </div>
                    <!-- /.wide-banner -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.wide-banners -->


        <!-- /.section -->
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

        <!-- ============================================== FEATURED PRODUCTS ============================================== -->
        <section class="section new-arriavls">
            <h3 class="section-title">Featured Products</h3>
            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p10.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p10_hover.jpg') }}" alt=""class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag new"><span>new</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price"> <span class="price"> $450.99 </span> <span
                                        class="price-before-discount">$ 800</span> </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i
                                                    class="fa fa-shopping-cart"></i> </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html"
                                                title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                        <li class="lnk"> <a class="add-to-cart" href="detail.html"
                                                title="Compare"> <i class="fa fa-signal" aria-hidden="true"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p2.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p2_hover.jpg') }}" alt=""
                                        class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag new"><span>new</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price">
                                    <span class="price"> $450.99 </span>
                                    <span class="price-before-discount">$ 800</span>
                                </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist">
                                            <a class="add-to-cart" href="#" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                            </a>
                                        </li>
                                        <li class="lnk">
                                            <a class="add-to-cart" href="detail.html"title="Compare">
                                                <i class="fa fa-signal" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->

                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p3.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p3_hover.jpg') }}" alt=""class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag hot"><span>hot</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price">
                                    <span class="price"> $450.99 </span>
                                    <span class="price-before-discount">$ 800</span>
                                </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist">
                                            <a class="add-to-cart" href="detail.html" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                            </a>
                                        </li>
                                        <li class="lnk"> <a class="add-to-cart" href="detail.html"title="Compare">
                                            <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->

                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p1.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p1_hover.jpg') }}" alt=""
                                        class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag hot"><span>hot</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price"> <span class="price"> $450.99 </span> <span
                                        class="price-before-discount">$ 800</span> </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist">
                                            <a class="add-to-cart" href="detail.html" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                            </a>
                                        </li>
                                        <li class="lnk">
                                            <a class="add-to-cart" href="detail.html"title="Compare">
                                                <i class="fa fa-signal" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p7.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p7_hover.jpg') }}" alt=""
                                        class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag sale"><span>sale</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price"> <span class="price"> $450.99 </span> <span
                                        class="price-before-discount">$ 800</span> </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i
                                                    class="fa fa-shopping-cart"></i> </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html"
                                                title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                        <li class="lnk"> <a class="add-to-cart" href="detail.html"
                                                title="Compare"> <i class="fa fa-signal" aria-hidden="true"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="detail.html">
                                        <img src="{{ asset('front-assets/images/products/p9.jpg') }}" alt="">
                                        <img src="{{ asset('front-assets/images/products/p9_hover.jpg') }}" alt=""
                                        class="hover-image">
                                    </a>
                                </div>
                                <!-- /.image -->
                                <div class="tag sale"><span>sale</span></div>
                            </div>
                            <!-- /.product-image -->
                            <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price"> <span class="price"> $450.99 </span> <span
                                        class="price-before-discount">$ 800</span> </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    <ul class="list-unstyled">
                                        <li class="add-cart-button btn-group">
                                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i
                                                    class="fa fa-shopping-cart"></i> </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                        </li>
                                        <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html"
                                                title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                        <li class="lnk"> <a class="add-to-cart" href="detail.html"
                                                title="Compare"> <i class="fa fa-signal" aria-hidden="true"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.action -->
                            </div>
                            <!-- /.cart -->
                        </div>
                        <!-- /.product -->
                    </div>
                    <!-- /.products -->
                </div>
                <!-- /.item -->
            </div>
            <!-- /.home-owl-carousel -->
        </section>
        <!-- /.section -->
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
    </div>

@endsection
