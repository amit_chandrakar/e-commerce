@extends('layouts.front')

@section('title')

Reset Password

@endsection
@section('body')

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{ route('front') }}">Home</a></li>
				<li class='active'>Reset Password</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div  iv class="container custom" >
		<div class="sign-in-page">
			<div class="row">
            <!-- create a new account -->
                <div class="col-md-12 col-sm-12 create-new-account">
                    <h4 class="checkout-subtitle">Reset Your Password</h4>
                    <form class="register-form outer-top-xs" role="form"  method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <div class="form-group">
                            <label class="info-title" for="address">{{ __('E-Mail Address') }} <span class="text-danger">*</span></label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="info-title" for="password">{{ __('Password') }} <span class="text-danger">*</span></label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="info-title" for="confirmPassword">{{ __('Confirm Password') }} <span class="text-danger">*</span></label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            @error('confirmPassword')
                            <span class="text-danger"> {{ $message }} </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary"> {{ __('Reset Password') }}</button>
                    </form>


                </div>
		    </div><!-- /.row -->
		</div><!-- /.sigin-in-->
</div><!-- /.body-content -->
@endsection

