@extends('layouts.base')

@section('title')
    Edit
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">Users</a>
                </li>
                <li class="active">
                    <strong>Edit</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-primary mt-3" id="save-data" style="margin-top: 3rem"> <span class="fa fa-save"></span>
                Save User</button>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Personal Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter name" class="form-control" name="name"
                                            id="name" value="{{ $user->name }}">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group @error('email') has-error @enderror">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" placeholder="Enter email" name="email"
                                            id="email" value="{{ $user->email }}">
                                        @error('email')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('password') has-error @enderror">
                                        <label for="password">Password <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" placeholder="Enter password"
                                            name="password" id="password" value="{{ $user->password }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('phone') has-error @enderror">
                                        <label for="phone">Phone No. <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" placeholder="Enter phone no"
                                            name="phone" id="phone" value="{{ $user->phone }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option>Select</option>
                                            <option value="active" {{ $user->status == 'active' ? 'selected' : '' }}>
                                                active
                                            </option>
                                            <option value="inactive" {{ $user->status == 'inactive' ? 'selected' : '' }}>
                                                inactive

                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Adresses</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">



                                <div class="col-md-12">
                                    <div class="form-group @error('country') has-error @enderror">
                                        <label for="country">Country <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter country" name="country"
                                            id="country" value="{{ $user->country }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('state') has-error @enderror">
                                        <label for="state">State <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter state" name="state"
                                            id="state" value="{{ $user->state }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('city') has-error @enderror">
                                        <label for="city">City <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter city" name="city"
                                            id="city" value="{{ $user->city }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('pincode') has-error @enderror">
                                        <label for="pincode">Pincode</label>
                                        <input type="number" class="form-control" placeholder="Enter pincode" name="pincode"
                                            id="pincode" value="{{ $user->pincode }}">
                                        @error('pincode')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('area') has-error @enderror">
                                        <label for="area">Area</label>
                                        <input type="text" class="form-control" placeholder="Enter area" name="area"
                                            id="area" value="{{ $user->area }}">
                                        @error('area')
                                            <span class="text-danger">{{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('landamark') has-error @enderror">
                                        <label for="landamark">landamark</label>
                                        <input type="text" class="form-control" placeholder="Enter landamark"
                                            name="landamark" id="landamark" value="{{ $user->landamark }}">
                                        @error('landamark')
                                            <span class="text-danger">{{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Uploaded Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file"
                                data-default-file="{{ asset('user-uploads/users/' . $user->photo) }}" />
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $('.dropify').dropify();

        $('.datepicker').datepicker({
            // format: 'yyyy-mm-dd',
            format: 'dd-mm-yyyy',
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })
    </script>

@endsection

@endsection
