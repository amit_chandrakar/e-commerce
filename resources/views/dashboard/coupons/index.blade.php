@extends('layouts.base')

@section('title')
    Coupon
@endsection
@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>E-commerce Coupon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Coupon</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="/create_coupon" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add Coupon</a>
        </div>
    </div>
@endsection
@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Coupon List
                        </h5>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover users-table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Discount</th>
                                        <th>Description</th>
                                        <th>Coupon Code</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mobile</td>
                                        <td>30%</td>
                                        <td>lorem12</td>
                                        <td>AD#3645</td>
                                        <td>
                                            <span class="label label-primary">Active</span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="#" class="btn-white btn btn-xs">View</a>

                                                <a href="#" class="btn-white btn btn-xs">Edit</a>

                                                <a href="" class="btn-white btn btn-xs delete-user">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile</td>
                                        <td>30%</td>
                                        <td>lorem12</td>
                                        <td>AD#3645</td>
                                        <td>
                                            <span class="label label-primary">Active</span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="#" class="btn-white btn btn-xs">View</a>

                                                <a href="#" class="btn-white btn btn-xs">Edit</a>

                                                <a href="" class="btn-white btn btn-xs delete-user">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile</td>
                                        <td>30%</td>
                                        <td>lorem12</td>
                                        <td>AD#3645</td>
                                        <td>
                                            <span class="label label-primary">Active</span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="#" class="btn-white btn btn-xs">View</a>

                                                <a href="#" class="btn-white btn btn-xs">Edit</a>

                                                <a href="" class="btn-white btn btn-xs delete-user">Delete</a>
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Mainly scripts -->
    <script src="{{ asset('js/dashboard.js') }}"></script>

    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

@endsection
