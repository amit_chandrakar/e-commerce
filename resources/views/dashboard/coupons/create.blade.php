@extends('layouts.base')

@section('title')
    Coupon
@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Coupon Form</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">Coupon</a>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>
    </div>
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Coupon Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group @error('title') has-error @enderror">
                                        <label for="title">Title <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter title" class="form-control" name="title"
                                            id="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group @error('discount') has-error @enderror">
                                        <label for="discount">Discount <span class="text-danger">*</span></label>
                                        <input type="discount" class="form-control" placeholder="Enter discount"
                                            name="discount" id="discount" value="{{ old('discount') }}">
                                        @error('discount')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <input type="description" class="form-control" placeholder="Enter description"
                                            name="description" id="description" value="{{ old('description') }}">
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('code') has-error @enderror">
                                        <label for="code">Coupon Code <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" placeholder="Enter code" name="code"
                                            id="code" value="{{ old('code') }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection
