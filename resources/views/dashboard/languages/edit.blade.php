@extends('layouts.base')
@section('title')
    Edit Language
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Update Language Form</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/setting-index">Home</a>
                </li>
                <li class="active">
                    <strong>Edit Language</strong>
                </li>
            </ol>
        </div>
    </div>
    <body onload="myFunction()">
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLabel">Fill Form</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" class="form-horizontal" action="{{ route('languages.update', $languages->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{ $languages->name }}"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Code</label>
                                <div class="col-sm-10"><input type="text" class="form-control" name="code" value="{{ $languages->code }}"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Select</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="status" id="status">
                                                <option>Select</option>
                                                <option value="active" {{ $languages->status == 'active' ? 'selected' : '' }}>Active</option>
                                                <option value="inactive" {{ $languages->status == 'inactive' ? 'selected' : '' }}>Inactive</option>
                                     </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            function myFunction() {
                $('#exampleModal').modal('show');
            }
        </script>
    </body>
@endsection
