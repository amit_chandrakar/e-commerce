@extends('layouts.base')

@section('title')
    Product Index
@endsection

@section('css')
    <!-- DataTable -->
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Products </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="">Manage Product</a>
                </li>

                <li class="active">
                    <strong>Products</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{route ('products.create')}}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add Product</a>
        </div>
    </div>
@endsection
@section('content')

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Products List
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover users-table">
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)

                                    <tr>
                                        <td>
                                            <img src="{{ asset('products-uploads/products/'.$product->photo) }}" alt="" height="60"
                                                width="60">
                                        </td>
                                        <td>
                                           {{ $product->name }}
                                        </td>
                                        <td>
                                            {{ $product->quantity }}

                                        </td>
                                        <td>
                                            {{ $product->price }}

                                        </td>
                                        <td>
                                            {{ $product->description }}
                                        </td>
                                        <td>
                                            @if ($product->status == 'active')
                                                <span class="label label-primary">Active</span>
                                            @else
                                                <span class="label label-danger">In-Active</span>
                                            @endif
                                        </td>

                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('products.show',$product->id) }}" class="btn-white btn btn-xs">View</a>

                                                <a href="{{ route('products.edit',$product->id) }}" class="btn-white btn btn-xs">Edit</a>

                                                <a href="javascript:;" data-product-id="{{ $product->id }}" class="btn-white btn btn-xs delete-product">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Mainly scripts -->
    <script src="{{ asset('js/dashboard.js') }}"></script>

    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.users-table').DataTable();

            @if (session('success'))
                toastr.success('{{ session('success') }}', 'Success');
            @endif


            $('.delete-product').click(function() {

                let productId = $(this).data('product-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route('products.destroy', ':id') }}';
                url = url.replace(':id', productId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',
                        },
                        success: function() {
                            swal("Deleted!", "User account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>

@endsection
