@extends('layouts.base')

@section('title')
    Edit Products
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('asset/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">

    <link href="{{ asset('asset/css/plugins/dropzone/basic.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2> Products </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="{{ route('products.index') }}">Manage Product</a>
                </li>
                <li>
                    <a href="{{ route('products.index') }}">Products</a>
                </li>

                <li class="active">
                    <strong>Edit Products</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-3">
            <a href="{{ route('products.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
             <button class="btn btn-primary mt-3" id="save-data">
                    <span class="fa fa-save"></span> Edit & Save </button>
        </div>

    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('products.update',$products->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Product Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group @error('category_id') has-error @enderror">
                                        <label for="category_id">Category <span class="text-danger">*</span></label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            <option>Select</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"@if($category->id == $products->id) selected @endif  >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="category_id">Sub-Category </label>
                                        <select class="form-control" name="sub_category_id" id="sub_category_id" >
                                            @foreach ($subCategories as $subCategory)
                                                <option value="{{ $subCategory->id }}"@if($subCategory->id == $products->id) selected @endif  >{{ $subCategory->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group @error('product_name') has-error @enderror">
                                        <label for="product_name">Product Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter product_name" class="form-control" name="name"
                                            id="product_name" value="{{ $products->name }}">
                                        @error('product_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('price') has-error @enderror">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="price" class="form-control" placeholder="Enter price" name="price"
                                            id="price" value="{{ $products->price }}">
                                        @error('price')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('quantity') has-error @enderror">
                                        <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                        <input type="quantity" class="form-control" placeholder="Enter quantity"
                                            name="quantity" id="quantity" value="{{ $products->quantity }}">
                                        @error('quantity')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter desription"
                                            name="description" id="description" value="{{ $products->description }}">
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option>Select</option>
                                            <option value="active"
                                                {{ $products->status == 'active' ? 'selected' : '' }}>active</option>
                                            <option value="inactive"
                                                {{ $products->status == 'inactive' ? 'selected' : '' }}>inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

            </form>


        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- ck editor -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- SUMMERNOTE -->
    <script src="{{ asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>

    <!-- DROPZONE -->
    <script src="{{ asset('asset/js/plugins/dropzone/dropzone.js') }}"></script>
    <script>
        $('.dropify').dropify();


        $(document).ready(function() {
            Dropzone.options.myAwesomeDropzone = {
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element
                        .querySelector("button[type=submit]")
                        .addEventListener("click", function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            myDropzone.processQueue();
                        });
                    this.on("sendingmultiple", function() {});
                    this.on("successmultiple", function(files, response) {});
                    this.on("errormultiple", function(files, response) {});
                },
            };
        });




        $('.summernote').summernote();

        var edit = function() {
            $('.click2edit').summernote({
                focus: true
            });
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })

        $(document).on('change', '#category_id', function() {
            var categoryId = $(this).val();
            let url = "{{ route('get_sub_categories', ':id') }}";
            url = url.replace(':id', categoryId);

            $.ajax({
                url: url,
                type: 'GET',
                success: function(response) {
                    $('#sub_category_id').html(response);
                }
            });

         });

    </script>

@endsection
