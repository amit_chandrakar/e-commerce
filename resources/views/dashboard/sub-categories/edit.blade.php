@extends('layouts.base')

@section('title')
    Edit Sub-Category
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

@endsection
@section('breadcrum')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit Sub-Categories</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Manage Product</a>
                </li>
                <li>
                    <a href="#">Sub-Categories</a>
                </li>

                <li class="active">
                    <strong>Edit Sub-Categories </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('sub-categories.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
                <button class="btn btn-primary mt-3" id="save-data"> <span
                            class="fa fa-save"></span> Edit & Save</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('sub-categories.update', $SubCategories->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Product Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Category <span class="text-danger">*</span></label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"@if($category->id == $SubCategories->id) selected @endif>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('product_sub_category') has-error @enderror">
                                        <label for="product_sub_category">Product Category <span
                                                class="text-danger">*</span></label>
                                        <input type="product_sub_category" class="form-control"
                                            placeholder="Enter product_sub_category" name="name" id="product_sub_category"
                                            value="{{ $SubCategories->name }}">
                                        @error('product_sub_category')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option>Select</option>
                                            <option value="active"
                                                {{ $SubCategories->status == 'active' ? 'selected' : '' }}>active
                                            </option>
                                            <option value="inactive"
                                                {{ $SubCategories->status == 'inactive' ? 'selected' : '' }}>inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Uploaded Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file"
                                data-default-file="{{ asset('sub-categories-uploads/sub-categories/' . $SubCategories->photo) }}" />
                        </div>
                    </div>
                    <div>
                    </div>
                </div>

            </form>
        </div>
    </div>


@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Switchery -->
    <script src="{{ asset('asset/js/plugins/switchery/switchery.js') }}"></script>

    <script>
        $('.dropify').dropify();

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })
    </script>

@endsection
