@extends('layouts.base')

@section('title')
    Product Index
@endsection

@section('css')
    <!-- DataTable -->
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Products </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">Manage Product</a>
                </li>

                <li class="active">
                    <strong>Products</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="/test_create" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add Product</a>
        </div>
    </div>
@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Products List
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover users-table">
                            <thead>
                                <tr>
                                    <th>Product Image</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quanlity</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            image
                                        </td>
                                        <td>Mobile</td>
                                        <td>15,999</td>
                                        <td>2</td>
                                        <td>lorem</td>
                                        <td>
                                                <label class="label label-primary">Active</label>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="test_show"
                                                    class="btn-white btn btn-xs">View</a>

                                                <a href="test_edit"
                                                    class="btn-white btn btn-xs">Edit</a>

                                                <a href="javascript:;"
                                                    class="btn-white btn btn-xs delete-subcategory">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <!-- Mainly scripts -->
    <script src="{{ asset('js/dashboard.js') }}"></script>

    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

@endsection
