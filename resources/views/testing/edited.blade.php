@extends('layouts.base')

@section('title')
    Edit Products
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('asset/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">

    <link href="{{ asset('asset/css/plugins/dropzone/basic.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2> Products </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">Manage Product</a>
                </li>
                <li>
                    <a href="">Products</a>
                </li>

                <li class="active">
                    <strong>Edit Products</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-3">
            <a href="/products_index" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
            <a herf="/products_index"> <button class="btn btn-primary mt-3" id="save-data">
                    <span class="fa fa-save"></span> Edit & Save </button> </a>
        </div>

    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Product Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group @error('product_name') has-error @enderror">
                                        <label for="product_name">Product Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter product_name" class="form-control"
                                            product_name="product_name" id="product_name"
                                            value="{{ old('product_name') }}">
                                        @error('product_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('price') has-error @enderror">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="price" class="form-control" placeholder="Enter price" name="price"
                                            id="price" value="{{ old('price') }}">
                                        @error('price')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('quality') has-error @enderror">
                                        <label for="quality">Quality <span class="text-danger">*</span></label>
                                        <input type="quality" class="form-control" placeholder="Enter quality"
                                            name="quality" id="quality" value="{{ old('quality') }}">
                                        @error('quality')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option>Select</option>
                                            <option value="active">active
                                            </option>
                                            <option value="inactive">inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Description</h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content no-padding">
                                                <div class="summernote">
                                                    <h3>Lorem Ipsum is simply</h3>
                                                    dummy text of the printing and typesetting industry. <strong>Lorem Ipsum
                                                        has been the industry's</strong> standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a
                                                    type specimen book. It has survived not only five centuries, but also
                                                    the leap into electronic typesetting, remaining essentially unchanged.
                                                    It was popularised in the 1960s with the release of Letraset sheets
                                                    containing
                                                    Lorem Ipsum passages, and more recently with
                                                    <br />
                                                    <br />
                                                    <ul>
                                                        <li>Remaining essentially unchanged</li>
                                                        <li>Make a type specimen book</li>
                                                        <li>Unknown printer</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

            </form>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Multiple Images</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <form id="my-awesome-dropzone" class="dropzone" action="#">
                        <div class="dropzone-previews"></div>
                        <button type="submit" class="btn btn-primary pull-right">
                            Submit this form!
                        </button>
                    </form>
                    <div>
                        <div class="m text-right">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('script')

    <script src="{{ asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- ck editor -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- SUMMERNOTE -->
    <script src="{{ asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>

    <!-- DROPZONE -->
    <script src="{{ asset('asset/js/plugins/dropzone/dropzone.js') }}"></script>

    <script>
        $('.dropify').dropify();


        $(document).ready(function() {
            Dropzone.options.myAwesomeDropzone = {
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element
                        .querySelector("button[type=submit]")
                        .addEventListener("click", function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            myDropzone.processQueue();
                        });
                    this.on("sendingmultiple", function() {});
                    this.on("successmultiple", function(files, response) {});
                    this.on("errormultiple", function(files, response) {});
                },
            };
        });




        $('.summernote').summernote();

        var edit = function() {
            $('.click2edit').summernote({
                focus: true
            });
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })
    </script>

@endsection
