<?php

namespace App\Observers;

use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ProductsObserver
{
    public function creating(Product $products)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $products->photo = $file_name; // Save file name to database
        }

        $products->slug = Str::slug($products->name, '-');
    }

    public function created(Product $products)
    {
        if (request()->file) {
            $path = public_path('products-uploads/products');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $products->photo);
        }

    }

    public function saving(Product $products)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('products-uploads/products/');
            $this->deleteFile($path . $products->photo);

            $file_name = time() . '.' . request()->file->extension();
            $products->photo = $file_name; // Save file name to database
        }

        $products->slug = Str::slug($products->name, '-');
    }

    public function updated(Product $products)
    {
        if (request()->file) {

            $path = public_path('products-uploads/products/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $products->photo);
        }
    }

    public function deleted(Product $products)
    {
        $path = public_path('products-uploads/products/');

        // Old file delete code
        $this->deleteFile($path . $products->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }

}
