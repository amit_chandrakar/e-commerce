<?php

namespace App\Observers;

use App\Models\Categories;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;


class CategoriesObserver
{
    public function creating(Categories $categories)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $categories->photo = $file_name; // Save file name to database
        }
        $categories->slug = Str::slug($categories->name, '-');
    }

    /**
     * Handle the Categories "created" event.
     *
     * @param  \App\Models\Categories  $Categories
     * @return void
     */
    public function created(Categories $categories)
    {
        if (request()->file) {
            $path = public_path('categories-uploads/categories');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $categories->photo);
        }

    }

    public function saving(Categories $categories)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('categories-uploads/categories/');
            $this->deleteFile($path . $categories->photo);

            $file_name = time() . '.' . request()->file->extension();
            $categories->photo = $file_name; // Save file name to database
        }
        $categories->slug = Str::slug($categories->name, '-');

    }

    /**
     * Handle the Categories "updated" event.
     *
     * @param  \App\Models\Categories  $Categories
     * @return void
     */
    public function updated(Categories $categories)
    {
        if (request()->file) {

            $path = public_path('categories-uploads/categories/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $categories->photo);
        }
    }

    /**
     * Handle the Categories "deleted" event.
     *
     * @param  \App\Models\Categories  $Categories
     * @return void
     */
    public function deleted(Categories $categories)
    {
        $path = public_path('categories-uploads/categories/');

        // Old file delete code
        $this->deleteFile($path . $categories->photo);
    }

    /**
     * Handle the Categories "restored" event.
     *
     * @param  \App\Models\Categories  $Categories
     * @return void
     */
    public function restored(Categories $categories)
    {
        //
    }

    /**
     * Handle the Categories "force deleted" event.
     *
     * @param  \App\Models\Categories  $Categories
     * @return void
     */
    public function forceDeleted(Categories $categories)
    {
        //
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}
