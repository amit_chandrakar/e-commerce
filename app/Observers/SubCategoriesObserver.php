<?php

namespace App\Observers;

use App\Models\SubCategory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class SubCategoriesObserver
{
    public function creating(SubCategory $SubCategories)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $SubCategories->photo = $file_name; // Save file name to database
        }

        $SubCategories->slug = Str::slug($SubCategories->name, '-');
    }

    public function created(SubCategory $SubCategories)
    {
        if (request()->file) {
            $path = public_path('sub-categories-uploads/sub-categories');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $SubCategories->photo);
        }
    }

    public function saving(SubCategory $SubCategories)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('sub-categories-uploads/sub-categories/');
            $this->deleteFile($path . $SubCategories->photo);

            $file_name = time() . '.' . request()->file->extension();
            $SubCategories->photo = $file_name; // Save file name to database
        }

        $SubCategories->slug = Str::slug($SubCategories->name, '-');
    }

    public function updated(SubCategory $SubCategories)
    {
        if (request()->file) {

            $path = public_path('sub-categories-uploads/sub-categories/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $SubCategories->photo);
        }
    }

    public function deleted(SubCategory $SubCategories)
    {
        $path = public_path('sub-categories-uploads/sub-categories/');

        // Old file delete code
        $this->deleteFile($path . $SubCategories->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }

}
