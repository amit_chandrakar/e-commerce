<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Categories;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::get();
        return view('dashboard.products.index', compact('products'));
    }

    public function create()
    {
        $categories = Categories::get();
        $subCategories = SubCategory::get();

        return view('dashboard.products.create', compact('categories', 'subCategories'));
    }

    // public function store(StoreProductRequest $request)
    public function store(Request $request)
    {
        $products = new Product;
        $products->category_id = $request->category_id;
        $products->sub_category_id = $request->sub_category_id;
        $products->name = $request->name;
        $products->price = $request->price;
        $products->quantity = $request->quantity;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->save();

        return redirect('products')->with(['success' => 'Product added.']);
    }


    public function show($id)
    {
        $products = Product::find($id);
        $categories = Categories::get();
        $subCategories = SubCategory::get();
        return view('dashboard.products.show', compact('products', 'categories', 'subCategories'));
    }


    public function edit($id)
    {
        $products = Product::find($id);
        $categories = Categories::get();
        $subCategories = SubCategory::get();

        return view('dashboard.products.edit', compact('products', 'categories', 'subCategories'));
    }

    public function update(Request $request, $id)
    {
        $products = Product::find($id);
        $products->name = $request->name;
        $products->price = $request->price;
        $products->quantity = $request->quantity;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->save();

        return redirect('/products')->with(['success' => 'User has been updated.']);
    }

    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect('/products')->with(['success' => 'User has been deleted.']);
    }
}
