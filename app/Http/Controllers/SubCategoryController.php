<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubCategoryRequest;
use App\Models\Categories;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $SubCategories = SubCategory::get();
        return view('dashboard.sub-categories.index', compact('SubCategories'));
    }

    public function create()
    {
        $categories = Categories::get();
        return view('dashboard.sub-categories.create', compact('categories'));
    }

    public function store(StoreSubCategoryRequest $request)
    {
        $SubCategories = new SubCategory;
        $SubCategories->category_id = $request->category_id;
        $SubCategories->name = $request->name;
        $SubCategories->status = $request->status;
        $SubCategories->save();

        return redirect('/sub-categories')->with(['success' => 'Sub-Category has been created.']);
    }

    public function show($id)
    {
        $SubCategories = SubCategory::find($id);
        $categories = Categories::get();

        return view('dashboard.sub-categories.show', compact('SubCategories', 'categories'));
    }

    public function edit($id)
    {
        $SubCategories = SubCategory::find($id);
        $categories = Categories::get();

        return view('dashboard.sub-categories.edit', compact('SubCategories', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $SubCategories = SubCategory::find($id);
        $SubCategories->name = $request->name;
        $SubCategories->status = $request->status;
        $SubCategories->save();

        return redirect('/sub-categories')->with(['success' => 'User has been updated.']);
    }

    public function destroy($id)
    {
        SubCategory::find($id)->delete();

        return redirect('/sub-categories')->with(['success' => 'User has been deleted.']);
    }

    public function getSubcategories($categoryId)
    {
        $SubCategories = SubCategory::where('category_id', $categoryId)->get();

        $options = '<option>--select--</option>';

        foreach ($SubCategories as $SubCategory) {
            $options .= '<option value="' . $SubCategory->id . '">' . $SubCategory->name . '</option>';
        }

        // return Response($options);
        return $options;
    }
}
