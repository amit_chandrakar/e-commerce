<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoriesRequest;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Categories::get();
        return view('dashboard.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function store(StoreCategoriesRequest $request)
    {
        $categories = new Categories;
        $categories->name = $request->name;
        $categories->status = $request->status;
        $categories->save();

        return redirect('/categories')->with(['success' => 'User has been created.']);
    }

    public function show($id)
    {
        $categories = Categories::find($id);
        return view('dashboard.categories.show', compact('categories'));
    }

    public function edit($id)
    {
        $categories = Categories::find($id);
        return view('dashboard.categories.edit', compact('categories'));
    }

    public function update(Request $request, $id)
    {
        $categories = Categories::find($id);
        $categories->name = $request->name;
        $categories->status = $request->status;
        $categories->save();
        return redirect('/categories')->with(['success' => 'User has been updated.']);
    }


    public function destroy($id)
    {
        Categories::find($id)->delete();
        return redirect('/categories')->with(['success' => 'User has been deleted.']);
    }


}
