<?php

namespace App\Http\Controllers;

use App\Models\LanguageSetting;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\Language;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LanguageSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = LanguageSetting::get();
        return view('dashboard.settings.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = new LanguageSetting;
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;
        $path = base_path() . '/resources/lang/' . $request->code;
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $languages->save();
        return redirect('/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = LanguageSetting::find($id);
        return view('dashboard.languages.edit', compact('languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $languages = LanguageSetting::find($id);
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;
        $languages->save();
        return redirect('/settings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = LanguageSetting::select('code')->where('id',$id )->pluck('code');
        // LanguageSetting::where('age', '29')->pluck('name');
        // dd($code[0]);
        $path = base_path().'/resources/lang/'.$code[0];
        // dd($path);
        if (file_exists($path)) {
              File::deleteDirectory($path);
          }
        LanguageSetting::find($id)->delete();
        return redirect('/settings');

    }
}
