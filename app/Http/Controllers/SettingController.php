<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\GeneralSetting;
use App\Models\LanguageSetting;
use App\Models\SocialLink;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index(){
        $generalSettings = GeneralSetting::first();
        $socialLinks = SocialLink::first();
        $faqs = Faq::get();
        $languages = LanguageSetting::get();
        return view('dashboard.settings.index', compact('generalSettings', 'socialLinks', 'faqs', 'languages'));
    }
}
