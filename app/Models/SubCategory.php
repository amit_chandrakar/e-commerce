<?php

namespace App\Models;

use App\Observers\SubCategoriesObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'name',
        'slug',
        'photo',
        'tags',
        'status',
        'created_by',
        'updated_by',
    ];

    public function category(){
        return $this ->belongsTo(Categories::class);
    }


    protected static function boot()
    {
        parent::boot();
        static::observe(SubCategoriesObserver::class);
    }
}
