<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    use HasFactory;
    protected $fillable = [
        'facebook_url',
       'twitter_url',
       'pinterest_url',
       'youtube_url',
       'linkedin_url',
       'created_by',
       'updated_by',
    ];
}
