<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmtpSettings extends Model
{
    use HasFactory;
    
     protected $fillable = [
     'mailer',
    'host',
    'username',
    'password',
    'encryption',
    'sender_name',
    'sender_email',
    'status',
    'created_by',
    'updated_by',
     ];
}
