<?php

namespace App\Models;

use App\Observers\CategoriesObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'photo',
        'status',
        'created_by',
        'updated_by',
    ];

    public function subcategory(){
        return $this ->hasMany(SubCategory::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::observe(CategoriesObserver::class);
    }
}
