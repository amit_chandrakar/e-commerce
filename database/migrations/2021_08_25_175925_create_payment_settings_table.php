<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_settings', function (Blueprint $table) {
           $table->id();
            $table->string('paypal_key');
            $table->string('paypal_secret');
            $table->enum('paypal_status',['active','inactive']);
            $table->string('razorpay_key');
            $table->string('razorpay_secret');
            $table->enum('razorpay_status',['active','inactive']);
            $table->string('stripe_key');
            $table->string('stripe_secret');
            $table->enum('stripe_status',['active','inactive']);
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_settings');
    }
}
