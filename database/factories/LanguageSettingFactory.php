<?php

namespace Database\Factories;

use App\Models\LanguageSetting;
use Illuminate\Database\Eloquent\Factories\Factory;

class LanguageSettingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LanguageSetting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
