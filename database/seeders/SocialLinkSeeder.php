<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class SocialLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
 DB::table('social_links')->insert(array(
       array(
                'facebook_url'=>'www.facebook.com',
                'twitter_url'=>'www.twitter.com',
                'pinterest_url' => 'www.pinterest.com',
                'youtube_url'=>'www.youtube.com',
                'linkedin_url'=>'www.linkedin.com',
                


       )));

    }
}
