<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->insert(array(
       array(
                'title'=>'asking about warranty of products',
                'description'=>'please tell me the warranty of products',
               'status'=>'active'

       )));

    }
}
