<?php
namespace Database\Seeders;
use App\Models\Categories;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ([
            [
                'id' => '1',
                'name' => 'Clothing',
                'status' => 'active'
            ],
            [
                 'id' => '2',
                'name' => 'Footwear',
                'status' => 'active'
            ],
            [
                 'id' => '3',
                'name' => 'Beauty',
                'status' => 'active'
            ],
            [
                 'id' => '4',
                'name' => 'Watches',
                'status' => 'active'
            ],
            [
                 'id' => '5',
                'name' => 'Jewellery',
                'status' => 'active'
            ],
            [
                 'id' => '6',
                'name' => 'Eyewear',
                'status' => 'active'
            ],
            [
                 'id' => '7',
                'name' => 'Wallets',
                'status' => 'active'
            ],
            [
                 'id' => '8',
                'name' => 'Hangbags & Clutches',
                'status' => 'active'
            ],
        ]);
        collect($categories)->each(function ($category) { Categories::create($category); });

    }
}
