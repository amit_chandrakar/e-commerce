<?php

namespace Database\Seeders;
use App\Models\Orders;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = ([
            [
                'id' => '1',
                'user_id'=>'1',
                'product_id' => '1',
                'status' => 'active'
            ],
        ]);
        collect($orders)->each(function ($orders) { Orders::create($orders); });


    }
}
