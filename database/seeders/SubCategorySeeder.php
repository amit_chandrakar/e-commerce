<?php

namespace Database\Seeders;
use App\Models\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = ([
            [
                'id'=>'1',
                'category_id'=>'1',
                'name' => 'T-Shirts & Polos',
                'status' => 'active'
            ],
            [
                'id'=>'2',
                'category_id'=>'1',
                'name' => 'Shirts',
                'status' => 'active'
            ],
            [
                'id'=>'3',
                'category_id'=>'1',
                'name' => 'Trousers',
                'status' => 'active'
            ],
            [
                'id'=>'4',
                'category_id'=>'1',
                'name' => 'Jeans',
                'status' => 'active'
            ],
            [
                'id'=>'5',
                'category_id'=>'1',
                'name' => 'Innerwear',
                'status' => 'active'
            ],
            [
                'id'=>'6',
                'category_id'=>'1',
                'name' => 'Sportswear',
                'status' => 'active'
            ],
            [
                'id'=>'7',
                'category_id'=>'1',
                'name' => 'Sleep & Lounge Wear',
                'status' => 'active'
            ],
            [
                'id'=>'8',
                'category_id'=>'1',
                'name' => 'Ethnic Wear',
                'status' => 'active'
            ],
            [
                'id'=>'9',
                'category_id'=>'1',
                'name' => 'Ties, Socks & Belts',
                'status' => 'active'
            ],
            [
                'id'=>'10',
                'category_id'=>'1',
                'name' => 'Suits & Blazers',
                'status' => 'active'
            ],
            [
                'id'=>'11',
                'category_id'=>'1',
                'name' => 'Sweaters',
                'status' => 'active'
            ],
            [
                'id'=>'12',
                'category_id'=>'1',
                'name' => 'Jackets & Coats',
                'status' => 'active'
            ],
            [
                'id'=>'13',
                'category_id'=>'1',
                'name' => 'Tops & Tees',
                'status' => 'active'
            ],
            [
                'id'=>'14',
                'category_id'=>'1',
                'name' => 'Kurtas',
                'status' => 'active'
            ],
            [
                'id'=>'15',
                'category_id'=>'1',
                'name' => 'Salwar Suits',
                'status' => 'active'
            ],
            [
                'id'=>'16',
                'category_id'=>'1',
                'name' => 'Sarees',
                'status' => 'active'
            ],
            [
                'id'=>'17',
                'category_id'=>'1',
                'name' => 'Pants',
                'status' => 'active'
            ],
            [
                'id'=>'18',
                'category_id'=>'2',
                'name' => ' Sports Shoes',
                'status' => 'active'
            ],
            [
                'id'=>'19',
                'category_id'=>'2',
                'name' => 'Formal Shoes',
                'status' => 'active'
            ],

            [
                'id'=>'20',
                'category_id'=>'2',
                'name' => 'Casual Shoes',
                'status' => 'active'
            ],
            [
                'id'=>'21',
                'category_id'=>'2',
                'name' => 'Sneakers',
                'status' => 'active'
            ],
            [
                'id'=>'22',
                'category_id'=>'2',
                'name' => 'Loafers & Mocassins',
                'status' => 'active'
            ],
            [
                'id'=>'23',
                'category_id'=>'2',
                'name' => 'Flip-Flops',
                'status' => 'active'
            ],
            [
                'id'=>'24',
                'category_id'=>'2',
                'name' => 'Boots',
                'status' => 'active'
            ],
            [
                'id'=>'25',
                'category_id'=>'2',
                'name' => 'Sandals & Floaters',
                'status' => 'active'
            ],
            [
                'id'=>'26',
                'category_id'=>'2',
                'name' => 'Thong Sandals',
                'status' => 'active'
            ],
            [
                'id'=>'27',
                'category_id'=>'2',
                'name' => 'Boat Shoes',
                'status' => 'active'
            ],
            [
                'id'=>'28',
                'category_id'=>'3',
                'name' => 'Skincare',
                'status' => 'active'
            ],
            [
                'id'=>'29',
                'category_id'=>'3',
                'name' => 'Haircare',
                'status' => 'active'
            ],
            [
                'id'=>'30',
                'category_id'=>'3',
                'name' => 'Face care',
                'status' => 'active'
            ],
            [
                'id'=>'31',
                'category_id'=>'3',
                'name' => 'Personal care',
                'status' => 'active'
            ],
            [
                'id'=>'32',
                'category_id'=>'4',
                'name' => 'Metallic',
                'status' => 'active'
            ],
            [
                'id'=>'33',
                'category_id'=>'4',
                'name' => 'Chronographs',
                'status' => 'active'
            ],
            [
                'id'=>'34',
                'category_id'=>'4',
                'name' => 'Leather',
                'status' => 'active'
            ],
            [
                'id'=>'35',
                'category_id'=>'4',
                'name' => 'Stainless Steel',
                'status' => 'active'
            ],
            [
                'id'=>'36',
                'category_id'=>'4',
                'name' => 'Smart Watches',
                'status' => 'active'
            ],
            [
                'id'=>'37',
                'category_id'=>'4',
                'name' => 'Fitness Bands',
                'status' => 'active'
            ],
            [
                'id'=>'38',
                'category_id'=>'5',
                'name' => 'Rings',
                'status' => 'active'
            ],
            [
                'id'=>'39',
                'category_id'=>'5',
                'name' => 'Bracelets',
                'status' => 'active'
            ],
            [
                'id'=>'40',
                'category_id'=>'5',
                'name' => 'Gold & Diamond Jewellery',
                'status' => 'active'
            ],
            [
                'id'=>'41',
                'category_id'=>'5',
                'name' => 'Silver Jewellery',
                'status' => 'active'
            ],
            [
                'id'=>'42',
                'category_id'=>'6',
                'name' => 'Sunglasses',
                'status' => 'active'
            ],
            [
                'id'=>'43',
                'category_id'=>'6',
                'name' => 'Spectacle Frame',
                'status' => 'active'
            ],
            [
                'id'=>'44',
                'category_id'=>'6',
                'name' => 'Goggles',
                'status' => 'active'
            ],
            [
                'id'=>'45',
                'category_id'=>'7',
                'name' => 'Leather',
                'status' => 'active'
            ],
            [
                'id'=>'46',
                'category_id'=>'7',
                'name' => 'Fabric',
                'status' => 'active'
            ],
            [
                'id'=>'47',
                'category_id'=>'7',
                'name' => 'Metal',
                'status' => 'active'
            ],
            [
                'id'=>'48',
                'category_id'=>'7',
                'name' => 'Polyester',
                'status' => 'active'
            ],
            [
                'id'=>'49',
                'category_id'=>'8',
                'name' => 'handbags',
                'status' => 'active'
            ],
        ]);
        collect($subcategories)->each(function ($subcategories) { SubCategory::create($subcategories); });

    }
}
