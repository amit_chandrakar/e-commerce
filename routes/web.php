<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\GeneralSettingController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\LanguageSettingController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\RazorpayController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SocialLinkController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Front Routes
Route::get('/', [FrontController::class, 'index'])->name('front');
Route::get('/detail/{slug}', [FrontController::class, 'productDetail']);
Route::get('faq', [FrontController::class, 'faq'])->name('faq');
Route::get('contact', [FrontController::class, 'contact'])->name('contact');
Route::get('shopping-cart', [FrontController::class, 'cart'])->name('cart');
Route::get('sign-in', [FrontController::class, 'signIn'])->name('sign-in');
Route::get('sign-up', [FrontController::class, 'signUp'])->name('sign-up');
Route::get('forgot-password', [FrontController::class, 'forgotPassword'])->name('forgot-password');
Route::get('reset-password', [FrontController::class, 'resetPassword'])->name('reset-password');

Auth::routes();

Route::get('/home', function () {
    return view('dashboard.dashboard.dashboard');
});

Route::resource('users', UserController::class);

Route::get('/coupons', function () {
    return view('dashboard.coupons.index');
});
Route::get('/create_coupon', function () {
    return view('dashboard.coupons.create');
});
Route::get('/test_index', function () {
    return view('dashboard.products.testing.indexed');
});
Route::get('/test_create', function () {
    return view('dashboard.products.testing.created');
});
Route::get('/test_show', function () {
    return view('dashboard.products.testing.showed');
});
Route::get('/test_update', function () {
    return view('dashboard.products.testing.edited');
});
Route::get('/orders', function () {
    return view('dashboard.orders.orders');
});
Route::get('/create_orders', function () {
    return view('dashboard.orders.create');
});
Route::get('/invoices', function () {
    return view('dashboard.invoices.index');
});

Route::get('/add_address', function () {
    return view('dashboard.settings.add_addresses');
});

Route::get('/add_languages', function () {
    return view('dashboard.settings.add_languages');
});
Route::get('/language-settings', function () {
    return view('dashboard.settings.language-settings.index');
});
Route::get('/smtp-settings', function () {
    return view('dashboard.settings.smtp-settings.create');
});
Route::get('/sms-settings', function () {
    return view('dashboard.settings.sms-settings.create');
});
Route::get('/addresses', function () {
    return view('dashboard.settings.addresses.index');
});
Route::get('/payments', function () {
    return view('dashboard.settings.payment-gateway-settings.create');
});
Route::get('settings', [SettingController::class, 'index'])->name('setting');
Route::resource('categories', CategoriesController::class);
Route::get('get-sub-categories/{categoryId}', [SubCategoryController::class, 'getSubcategories'])->name('get_sub_categories');
Route::resource('sub-categories', SubCategoryController::class);
Route::resource('products', ProductController::class);
Route::resource('users', UserController::class);
Route::resource('general-settings', GeneralSettingController::class);
Route::resource('languages', LanguageSettingController::class);
Route::resource('social-links', SocialLinkController::class);
Route::resource('faqs', FaqController::class);
/* Payment Gateways */

// Stripe Payment
Route::get('/stripe-payment', [StripeController::class, 'handleGet']);
Route::post('/stripe-payment', [StripeController::class, 'handlePost'])->name('stripe.payment');

// Razorpay Payment
Route::get('razorpay-payment', [RazorpayController::class, 'index']);
Route::post('razorpay-payment', [RazorpayController::class, 'store'])->name('razorpay.payment.store');
Route::get('/index', [FrontController::class, 'index']);

